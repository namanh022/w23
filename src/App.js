import logo from './logo.svg';
import './App.css';
import Reporter from './Reporter';

function App() {
   return (
    <div className="App">
      <img src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/f2fb2074-2033-4cc0-a701-c31ed2b53449/ddah93s-81f639d5-bb31-4264-b0dc-5f374fb7b10b.jpg/v1/fill/w_467,h_350,q_70,strp/groke_and_moomin_by_nonohara_susu_ddah93s-350t.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9OTYwIiwicGF0aCI6IlwvZlwvZjJmYjIwNzQtMjAzMy00Y2MwLWE3MDEtYzMxZWQyYjUzNDQ5XC9kZGFoOTNzLTgxZjYzOWQ1LWJiMzEtNDI2NC1iMGRjLTVmMzc0ZmI3YjEwYi5qcGciLCJ3aWR0aCI6Ijw9MTI4MCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.oEoRMUzupAkBmeA106cSY1eY38d2pCqgSwLOvrRrPKk"></img>
      <Reporter name="Antero Mertaranta">Löikö mörkö sisään</Reporter>
      <Reporter name="Abraham Lincoln">Whatever you are be a good one</Reporter>
    </div>
  );
}

export default App;
